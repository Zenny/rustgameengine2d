use components::component::*;
use entity::Details;
use components::gravity::*;

///
/// User input component makes an entity respond to user input,
/// specifically moving and jumping. Also deals with turning
/// physics.
///
pub struct UserInput {
    pub keystate: KeyState,
}

pub fn new() -> Box<Component> {
    Box::new(UserInput{
        keystate: KeyState::empty()
    })
}

impl Component for UserInput {
    fn run(&mut self, entity: &mut Details, delta: f64) -> bool {
        entity.vel[0] = self.keystate.accel(entity.vel[0], delta, entity.on_ground);
        if !entity.on_ground {
            if !self.keystate.contains(JUMP) { // < because it's negative for up
                entity.can_jump = true;

                if  entity.vel[1] < JUMP_MOD {
                    entity.vel[1] = JUMP_MOD;
                }
            }
        }
        else {
            if self.keystate.contains(JUMP) {
                if entity.can_jump {
                    entity.vel[1] = JUMP_MOD;
                    entity.on_ground = false;
                    entity.can_jump = false;
                }
            }
            else {
                entity.can_jump = true;
            }
        }
        entity.pos[0] += entity.vel[0] * delta;
        true
    }

    fn type_of(&self) -> &'static str {
        "UserInput"
    }

    fn get(&mut self) -> Type {
        Type::UserInput(self)
    }
}

impl UserInput {
    pub fn clear_keys(&mut self) {
        self.keystate = KeyState::empty();
    }
}


/* CONTROLS
Arrow keys - Move/aim
A - Jump
D - Shoot
F - Action
*/

bitflags! {
    pub flags KeyState: u8 { // No more room in u8
        const LEFT = 0b1,
        const RIGHT = 0b10,
        const UP = 0b100,
        const DOWN = 0b1000,
        const ACTION = 0b10000,
        const ATTACK = 0b100000,
        const JUMP = 0b1000000,
        const RUN = 0b10000000,
    }
}

impl KeyState {
    pub fn accel(&self, vel: f64, delta: f64, onground: bool) -> f64 {
        let mut acc = vel;
        if self.contains(LEFT) {
            if acc > -0.65 {
                acc -= VEL_DECAY / delta;
            }
                else {
                    acc -= MOD_ACCEL[0];
                }
        }
        if self.contains(RIGHT) {
            if acc < 0.65 {
                acc += VEL_DECAY / delta;
            }
                else {
                    acc += MOD_ACCEL[0];
                }
        }
        if (!self.contains(LEFT) && !self.contains(RIGHT)) || (self.contains(LEFT) && self.contains(RIGHT)) { // No buttons or both, decay
            if acc > 0.0 { acc -= VEL_DECAY / delta; }
                else if acc < 0.0 { acc += VEL_DECAY / delta; }
            if (acc > 0.0 && acc-(VEL_DECAY / delta) < 0.0) || (acc < 0.0 && acc+(VEL_DECAY / delta) > 0.0) {
                acc = 0.0;
            }
        }
        if acc > T_VEL[0]/1.8 {
            if self.contains(RUN) && self.contains(RIGHT) && onground {
                if acc > T_VEL[0] {
                    acc = T_VEL[0];
                }
            } else {
                acc -= (VEL_DECAY / delta)/3.95;//= T_VEL[0] / 1.8;
            }
        }
        else if acc < -T_VEL[0]/1.8 {
            if self.contains(RUN) && self.contains(LEFT) && onground {
                if acc < -T_VEL[0] {
                    acc = -T_VEL[0];
                }
            } else {
                acc += (VEL_DECAY / delta)/3.95;//= -T_VEL[0] / 1.8;
            }
        }
        acc
    }
}