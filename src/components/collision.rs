use components::component::*;
use entity::Details;
use screen::Screen;
use std::collections::HashMap;
use tile::Tile;

///
/// Collision component is in charge of collision between
/// entities and tiles.
///
pub struct Collision;

pub fn new() -> Box<Component> {
    Box::new(Collision{})
}

impl Component for Collision {
    fn run(&mut self, entity: &mut Details, delta: f64) -> bool {
        false
    }

    fn type_of(&self) -> &'static str {
        "Collision"
    }

    fn get(&mut self) -> Type {
        Type::Collision(self)
    }
}

impl Collision {

    ///
    /// Checks tiles surrounding an entity for collision.
    ///
    pub fn update(&mut self, entity: &mut Details, delta: f64, tiles: &mut HashMap<[u32; 2], Tile>,) {
        //self.collision(&entity, &tiles, delta);

        let x0 = ((entity.pos[0]/32.0).floor()*32.0) as u32;
        let x1 = (((entity.pos[0] + entity.size[0])/32.0).floor()*32.0) as u32;
        let mut y0 = ((entity.pos[1]/32.0).floor()*32.0) as u32;
        for i in 0..3 {
            let mut rem_tile = false;
            if i == 1 {
                y0 = (((entity.pos[1]+entity.size[1]/2.0)/32.0).floor()*32.0) as u32;
                /*[ ][ ]
                 *[-][-]
                 *[ ][ ]*/
            }
            if i == 2 {
                y0 = (((entity.pos[1]+entity.size[1]/2.0)/32.0).ceil()*32.0) as u32; // Ceil because negative is up?
                /*[ ][ ]
                 *[ ][ ]
                 *[-][-]*/
            }
            /*[x][ ]*/
            let mut pos = [x0,y0];
            {
                let mut tilew = tiles.get_mut(&pos);
                if tilew.is_some() {
                    let mut tile = tilew.unwrap();
                    if tile.collide_x(entity, pos, delta) {
                        //println!("Collided 0,0");
                        rem_tile = tile.collectable;
                    }
                    if tile.collide_y(entity, pos, delta) {
                        //println!("Collided 0,0");
                        rem_tile = tile.collectable;
                    }
                }
            }
            if rem_tile {
                rem_tile = false;
                tiles.remove(&pos);
            }

            /*[ ][x]*/
            pos = [x1,y0];
            {
                let mut tilew = tiles.get_mut(&pos);
                if tilew.is_some() {
                    let mut tile = tilew.unwrap();
                    if tile.collide_x(entity, pos, delta) {
                        //println!("Collided 0,0");
                        rem_tile = tile.collectable;
                    }
                    if tile.collide_y(entity, pos, delta) {
                        //println!("Collided 0,0");
                        rem_tile = tile.collectable;
                    }
                }
            }
            if rem_tile {
                tiles.remove(&pos);
            }
        } // For

        if entity.vel[1] > 0.0 { // Confirmed for falling
            entity.on_ground = false;
        }
    }
}