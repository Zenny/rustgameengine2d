use components::*;
use entity::Details;

///
/// An enum used to coerce a component from the generic trait-type to its base type.
///
pub enum Type<'a> {
    UserInput(&'a mut userinput::UserInput),
    Collision(&'a mut collision::Collision),
    Gravity(&'a mut gravity::Gravity),
}

///
/// The trait that all components must fulfill.
///
pub trait Component {
    /// run() is run every frame for the entity it's attached to
    /// Returns true if it's done updating
    fn run(&mut self, entity: &mut Details, delta: f64) -> bool;

    /// A string to identify the type of the component
    fn type_of(&self) -> &'static str;

    /// Get an enum-type version of the component that can be converted into its "base type"
    fn get(&mut self) -> Type;
}
