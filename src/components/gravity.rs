use components::*;
use components::component::*;
use entity::Details;

pub const T_VEL: [f64; 2] = [315.02, 1250.55]; //5.75
pub const MOD_ACCEL: [f64; 2] = [7.0, 2600.55]; // Amount to accel by each frame 7.75
pub const VEL_DECAY: f64 = 0.18; // Decay of the velocity when turning
pub const JUMP_MOD: f64 = -690.55; // Negative is up

///
/// The gravity component adds physics to an entity. Making
/// it fall and other such things.
///
pub struct Gravity;

pub fn new() -> Box<Component> {
    Box::new(Gravity{})
}

impl Component for Gravity {
    fn run(&mut self, entity: &mut Details, delta: f64) -> bool {
        entity.vel[1] = self.grav(entity.vel[1], delta);
        entity.pos[1] += entity.vel[1] * delta;

        true
    }

    fn type_of(&self) -> &'static str {
        "Gravity"
    }

    fn get(&mut self) -> Type {
        Type::Gravity(self)
    }
}

impl Gravity {
    pub fn grav(&self, vel: f64, delta: f64) -> f64 {
        let mut ret = vel;
        ret += MOD_ACCEL[1] * delta;
        if ret > T_VEL[1]{
            ret = T_VEL[1];
        }
        ret
    }
}