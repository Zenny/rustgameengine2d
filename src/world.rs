use opengl_graphics::Texture;
use screen::Screen;

///
/// A world is made up of screens, and holds these screens
/// in a vector.
///
pub struct World<'a> {
    pub screens: &'a mut Vec<Screen>,
    pub current_screen: usize,  // Non-persistent screen stuff goes on in here because it's a clone
    pub tex1: Texture,
}