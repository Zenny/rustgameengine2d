use graphics::Context;
use opengl_graphics::GlGraphics;
use opengl_graphics::GlyphCache;
use entity::Details;

///
/// Renders debug text on the screen using the desired font glyphs
///
pub fn text(c: &Context, g: &mut GlGraphics, font: &mut GlyphCache) {
    use current_camera; let cam = current_camera();
    let cam = cam.borrow_mut();

    use current_fps; let fps = current_fps();
    let mut fps = fps.borrow_mut();


    use graphics::{text, Transformed};
    let snap: &Details = unsafe { &*cam.snap };

    if !(cam.snap.is_null()) {
        text::Text::new_color([1.0; 4], 12).draw(
            &format!("{}\rCSE[x: {:.2}/{:.2} y: {:.2}/{:.2} v: {:.2}|{:.2}]", fps.tick(),
                snap.pos[0], cam.pos[0], snap.pos[1], cam.pos[1], snap.vel[0], snap.vel[1]
            ),
            font, &c.draw_state,
            c.transform.trans(5.0,5.0).zoom(0.25), g
        ).is_ok();
    }
    else {
        text::Text::new_color([1.0; 4], 12).draw(
            &format!("{}\rEDT[x: {:.2} y: {:.2} ]", fps.tick(),
                cam.pos[0], cam.pos[1]
            ),
            font, &c.draw_state,
            c.transform.trans(5.0,5.0).zoom(0.25), g
        ).is_ok();
    }
}
