use tile::Tile;
use entity::Entity;
use graphics::Context;
use opengl_graphics::{GlGraphics, Texture};
use std::collections::HashMap;
use graphics::{ Image, Transformed, Rectangle };
use current_camera;

pub struct Screen {
        pub entities: Vec<Entity>,
        pub tiles: HashMap<[u32; 2], Tile>,
}

///
/// A screen is a portion of the world that holds tiles and entities.
///
impl Screen {

    ///
    /// Initializes which collision sides should exist for all tiles.
    /// Prevents sticking to walls and floors in some circumstances.
    ///
    pub fn prepare(&mut self) {
        for mut tile in self.tiles.to_owned() {
            let pos = tile.0;
            let mut otile: Tile = self.tiles.get_mut(&[pos[0], pos[1]]).unwrap().to_owned();

            {
                let right = self.tiles.get_mut(&[pos[0]+32, pos[1]]);
                if right.is_some() {
                    let ntile = right.unwrap();
                    if !(otile.collectable || ntile.collectable)
                        //(otile.col[0] || otile.col[2]) && (ntile.col[0] || ntile.col[2]) // top/bot
                        && otile.col[1] && ntile.col[3] && otile.ttype == ntile.ttype { // Sides touching
                        otile.col[1] = false;
                        ntile.col[3] = false;
                    }
                }
            }
            if pos[0] >= 32 {
                let left = self.tiles.get_mut(&[pos[0]-32, pos[1]]);
                if left.is_some() {
                    let ntile = left.unwrap();
                    if !(otile.collectable || ntile.collectable)
                        //(otile.col[0] || otile.col[2]) && (ntile.col[0] || ntile.col[2]) // top/bot
                        && otile.col[3] && ntile.col[1] && otile.ttype == ntile.ttype { // Sides touching
                        otile.col[3] = false;
                        ntile.col[1] = false;
                    }
                }
            }
            if pos[1] >= 32 {
                let top = self.tiles.get_mut(&[pos[0], pos[1]-32]);
                if top.is_some() {
                    let ntile = top.unwrap();
                    if !(otile.collectable || ntile.collectable)
                       // (otile.col[1] || otile.col[3]) && (ntile.col[1] || ntile.col[3]) // l/r
                        && otile.col[0] && ntile.col[2] && otile.ttype == ntile.ttype { // Sides touching
                        otile.col[0] = false;
                        ntile.col[2] = false;
                    }
                }
            }
            {
                let bot = self.tiles.get_mut(&[pos[0], pos[1]+32]);
                if bot.is_some() {
                    let ntile = bot.unwrap();
                    if !(otile.collectable || ntile.collectable)
                        //(otile.col[1] || otile.col[3]) && (ntile.col[1] || ntile.col[3]) // l/r
                        && otile.col[2] && ntile.col[0] && otile.ttype == ntile.ttype { // Sides touching
                        otile.col[2] = false;
                        ntile.col[0] = false;
                    }
                }
            }

            self.tiles.insert([pos[0], pos[1]], otile);
        }
        println!("Screen ready");
    }
/*
    pub pos: [u32; 2],
    pub tex: [u8; 2], // 255
    pub slope: [u8; 2], // if 0,0 then not slope
    pub col_size: [u8; 2],
    pub collectable: bool,
    pub col: [bool; 4]
    */
    ///
    /// Inserts a tile of type tiletype at tpos with ttex, with a slope of tslope,
    /// and a collision size of tcol_size
    /// Collectible if tcollect is true
    /// Sides enabled/disabled if ocol is defined
    ///
    pub fn insTile(&mut self, tpos: [u32;2], ttex: [u8;2], tslope: [u8;2], tcol_size: [u8;2],
                   tcollect: bool, ocol: Option<[bool;4]>, tiletype: u8) {
        let mut tcol = [true,true,true,true];
        if ocol.is_some() {
            tcol = ocol.unwrap();
        }

        self.tiles.insert(tpos, Tile {
           // pos: tpos,
            tex: ttex,
            slope: tslope,
            col_size: tcol_size,
            collectable: tcollect,
            col: tcol,
            ttype: tiletype
        });
    }

    ///
    /// Renders all tiles and entities within camera boundaries for the current screen
    ///
    pub fn render(&mut self, c: &Context, g: &mut GlGraphics, tex: &Texture) {
        let cam = current_camera();
        let cam = cam.borrow();
        //let snap: &Entity = unsafe { &*cam.snap };
        //println!("{} {}", cam.size[0]/2.0, cam.size[1]/2.0);

        let realcampos = [cam.pos[0]-cam.size[0]/2.0, cam.pos[1]-cam.size[1]/2.0];
        for entity in &mut self.entities {
            let within = [entity.details.pos[0] as f64 - realcampos[0] + 32.0, entity.details.pos[1] as f64 - realcampos[1] + 32.0];
            if within[0] > 0.0 && within[1] > 0.0 && within[0] < cam.size[0] + 64.0 && within[1] < cam.size[1] + 64.0 {
                Image::new()
                    .src_rect([0.0,0.0, entity.details.size[0], entity.details.size[1]]) // x y w h
                    .draw(
                        &entity.details.tex, &c.draw_state,
                        c.transform.trans(
                            entity.details.pos[0] - cam.pos[0],
                            entity.details.pos[1] - cam.pos[1] )
                            .trans(
                                cam.size[0]/2.0,
                                cam.size[1]/2.0 ),
                        //.trans(
                        //    -(self.player.entity.size[0]/2.0),
                        //    -(self.player.entity.size[1]/2.0) ),
                        g
                    );

                // Draw debug collision boxes
                let x0 = (entity.details.pos[0]/32.0).floor()*32.0;
                let x1 = ((entity.details.pos[0] + entity.details.size[0]-0.5)/32.0).floor()*32.0;
                let mut y0 = (entity.details.pos[1]/32.0).floor()*32.0;
                //Top collision:
                Rectangle::new_border([1.0,0.0,0.0,1.0], 0.5)
                    .draw(
                        [0.0, 0.0, 32.0, 32.0],
                        &c.draw_state,
                        c.transform
                            .trans(x0 - cam.pos[0],
                                   y0 - cam.pos[1]).trans(cam.size[0]/2.0, cam.size[1]/2.0),
                        g
                    );
                Rectangle::new_border([1.0,0.0,0.0,1.0], 0.5)
                    .draw(
                        [0.0, 0.0, 32.0, 32.0],
                        &c.draw_state,
                        c.transform
                            .trans(x1 - cam.pos[0],
                                   y0 - cam.pos[1]).trans(cam.size[0]/2.0, cam.size[1]/2.0),
                        g
                    );

                y0 = ((entity.details.pos[1]+entity.details.size[1]/2.0)/32.0).floor()*32.0; // Ceil because negative is up?
                // Middle collision:
                Rectangle::new_border([0.0,1.0,0.0,1.0], 0.75)
                    .draw(
                        [0.0, 0.0, 32.0, 32.0],
                        &c.draw_state,
                        c.transform
                            .trans(x0 - cam.pos[0],
                                   y0 - cam.pos[1]).trans(cam.size[0]/2.0, cam.size[1]/2.0),
                        g
                    );
                Rectangle::new_border([0.0,1.0,0.0,1.0], 0.75)
                    .draw(
                        [0.0, 0.0, 32.0, 32.0],
                        &c.draw_state,
                        c.transform
                            .trans(x1 - cam.pos[0],
                                   y0 - cam.pos[1]).trans(cam.size[0]/2.0, cam.size[1]/2.0),
                        g
                    );

                y0 = ((entity.details.pos[1]+entity.details.size[1]/2.0)/32.0).ceil()*32.0; // Ceil because negative is up?
                // Bottom collision:
                Rectangle::new_border([0.0,0.0,1.0,1.0], 0.5)
                    .draw(
                        [0.0, 0.0, 32.0, 32.0],
                        &c.draw_state,
                        c.transform
                            .trans(x0 - cam.pos[0],
                                   y0 - cam.pos[1]).trans(cam.size[0]/2.0, cam.size[1]/2.0),
                        g
                    );
                Rectangle::new_border([0.0,0.0,1.0,1.0], 0.5)
                    .draw(
                        [0.0, 0.0, 32.0, 32.0],
                        &c.draw_state,
                        c.transform
                            .trans(x1 - cam.pos[0],
                                   y0 - cam.pos[1]).trans(cam.size[0]/2.0, cam.size[1]/2.0),
                        g
                    );
            }

        }

        for (pos, tile) in &self.tiles {
            let within = [pos[0] as f64 - realcampos[0] + 32.0, pos[1] as f64 - realcampos[1] + 32.0];
            if within[0] > 0.0 && within[1] > 0.0 && within[0] < cam.size[0] + 64.0 && within[1] < cam.size[1] + 64.0 {
                Image::new()
                    .src_rect([32.0 * (tile.tex[0] as f64), 32.0 * (tile.tex[1] as f64),
                        32.0, 32.0]) // x y w h
                    .draw(
                        tex, &c.draw_state,
                        c.transform.trans((pos[0] as f64) - cam.pos[0], (pos[1] as f64) - cam.pos[1]).trans(cam.size[0] / 2.0, cam.size[1] / 2.0),
                        g
                    );
            }
        }


     /*   let mut rect = Rectangle::new_border([1.0, 0.3, 0.0, 1.0], 0.5);
        // Tile collision view (Laggy)
        for mut tile in self.tiles.to_owned() {

            let pos = tile.0;
            let within = [pos[0] as f64 - realcampos[0] + 32.0, pos[1] as f64 - realcampos[1] + 32.0];
            if within[0] > 0.0 && within[1] > 0.0 && within[0] < cam.size[0] + 64.0 && within[1]  < cam.size[1] + 64.0 {
                let mut otile: Tile = self.tiles.get_mut(&[pos[0], pos[1]]).unwrap().to_owned();
                if otile.col[0] {
                        rect.draw(
                            [0.0, 0.0, 32.0, 1.0],
                            &c.draw_state,
                            c.transform
                                .trans(pos[0] as f64 - cam.pos[0],
                                       pos[1] as f64 - cam.pos[1]).trans(cam.size[0] / 2.0, cam.size[1] / 2.0),
                            g
                        );
                }
                if otile.col[1] {
                    rect.draw(
                            [0.0, 0.0, 1.0, 32.0],
                            &c.draw_state,
                            c.transform
                                .trans(pos[0] as f64 - cam.pos[0] + 31.0,
                                       pos[1] as f64 - cam.pos[1]).trans(cam.size[0] / 2.0, cam.size[1] / 2.0),
                            g
                        );
                }
                if otile.col[2] {
                    rect.draw(
                            [0.0, 0.0, 32.0, 1.0],
                            &c.draw_state,
                            c.transform
                                .trans(pos[0] as f64 - cam.pos[0],
                                       pos[1] as f64 - cam.pos[1] + 31.0).trans(cam.size[0] / 2.0, cam.size[1] / 2.0),
                            g
                        );
                }
                if otile.col[3] {
                    rect.draw(
                            [0.0, 0.0, 1.0, 32.0],
                            &c.draw_state,
                            c.transform
                                .trans(pos[0] as f64 - cam.pos[0],
                                       pos[1] as f64 - cam.pos[1]).trans(cam.size[0] / 2.0, cam.size[1] / 2.0),
                            g
                        );
                }
            }
        } // For*/

    }
}
