use opengl_graphics::{TextureSettings, Filter, Texture};
use entity::Entity;
use screen::Screen;
use tile::Tile;
use std::collections::HashMap;

const T_VEL: [f64; 2] = [315.02, 1250.55]; //5.75
const MOD_ACCEL: [f64; 2] = [7.0, 2600.55]; // Amount to accel by each frame 7.75
const VEL_DECAY: f64 = 0.18; // Decay of the velocity when turning
const JUMP_MOD: f64 = -690.55; // Negative is up


/* CONTROLS
Arrow keys - Move/aim
A - Jump
D - Shoot
F - Action
*/

bitflags! {
    pub flags KeyState: u8 { // No more room in u8
        const LEFT = 0b1,
        const RIGHT = 0b10,
        const UP = 0b100,
        const DOWN = 0b1000,
        const ACTION = 0b10000,
        const ATTACK = 0b100000,
        const JUMP = 0b1000000,
        const RUN = 0b10000000,
    }
}

impl KeyState {
    pub fn accel(&self, vel: f64, delta: f64, onground: bool) -> f64 {
        let mut acc = vel;
        if self.contains(LEFT) {
            if acc > -0.65 {
                acc -= VEL_DECAY / delta;
            }
            else {
                acc -= MOD_ACCEL[0];
            }
        }
        if self.contains(RIGHT) {
            if acc < 0.65 {
                acc += VEL_DECAY / delta;
            }
            else {
                acc += MOD_ACCEL[0];
            }
        }
        if (!self.contains(LEFT) && !self.contains(RIGHT)) || (self.contains(LEFT) && self.contains(RIGHT)) { // No buttons or both, decay
            if acc > 0.0 { acc -= VEL_DECAY / delta; }
            else if acc < 0.0 { acc += VEL_DECAY / delta; }
            if (acc > 0.0 && acc-(VEL_DECAY / delta) < 0.0) || (acc < 0.0 && acc+(VEL_DECAY / delta) > 0.0) {
                acc = 0.0;
            }
        }
        if acc > T_VEL[0]/1.8 {
            if self.contains(RUN) && self.contains(RIGHT) && onground {
                if acc > T_VEL[0] {
                    acc = T_VEL[0];
                }
            } else {
                acc -= (VEL_DECAY / delta)/3.95;//= T_VEL[0] / 1.8;
            }
        }
        else if acc < -T_VEL[0]/1.8 {
            if self.contains(RUN) && self.contains(LEFT) && onground {
                if acc < -T_VEL[0] {
                    acc = -T_VEL[0];
                }
            } else {
                acc += (VEL_DECAY / delta)/3.95;//= -T_VEL[0] / 1.8;
            }
        }
        acc
    }

    pub fn grav(&self, vel: f64, delta: f64) -> f64 {
        let mut ret = vel;
        ret += MOD_ACCEL[1] * delta;
        if ret > T_VEL[1]{
            ret = T_VEL[1];
        }
        ret
    }
}

pub struct Player {
    pub entity: Entity,
    pub keystate: KeyState,
    pub can_jump: bool,
}

impl Player {
    pub fn clear_keys(&mut self) {
        self.keystate = KeyState::empty();
    }

    pub fn new(p: [f64; 2]) -> Player {
        Player {
            keystate: KeyState::empty(),
            entity: Entity {
                tex: Texture::new(0,0,0), /*::empty() doesn't work. Also see: set_char_default() */
                size: [32.0,64.0],
                pos: p,
                vel: [0.0, -0.0],
                on_ground: false,
                can_jump: false,
            },
        }
    }

    /* We run this after GL is ready, because Texture won't really work until it is. */
    pub fn set_char_default(&mut self) {
        use current_path; let path = current_path();
        let path = path.borrow_mut();
        //let assets = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();
        //println!("{:?}", &assets);
        let pl = path.join("pl.png");
        self.entity.tex = Texture::from_path(
            &pl,
            &TextureSettings::new().filter(Filter::Nearest)
        ).unwrap();
        println!("Loaded char");
    }

    pub fn collision(&mut self, tiles: &mut HashMap<[u32; 2], Tile>, delta: f64) {
    /*    let x0 = ((self.entity.pos[0]/32.0).floor()*32.0) as u32;
        let x1 = (((self.entity.pos[0] + self.entity.size[0])/32.0).floor()*32.0) as u32;
        let mut y0 = ((self.entity.pos[1]/32.0).floor()*32.0) as u32;
        //Top possible collision here
        for i in 0..3 {
            let mut rem_tile = false;
            if i == 1 {
                y0 = (((self.entity.pos[1]+self.entity.size[1]/2.0)/32.0).floor()*32.0) as u32;
                /*[ ][ ]
                 *[-][-]
                 *[ ][ ]*/
            }
            if i == 2 {
                y0 = (((self.entity.pos[1]+self.entity.size[1]/2.0)/32.0).ceil()*32.0) as u32; // Ceil because negative is up?
                /*[ ][ ]
                 *[ ][ ]
                 *[-][-]*/
            }
            /*[x][ ]*/
            let mut pos = [x0,y0];
            if let Some(tile) = tiles.get(&pos) {
                if self.entity.collide_x(tiles, tile, pos, delta) {
                   // println!("Collided 1,0");
                    rem_tile = tile.collectable;
                }
                if self.entity.collide_y(tiles, tile, pos, delta) {
                    //println!("Collided 1,0");
                    rem_tile = tile.collectable;
                }
            }
            if rem_tile {
                rem_tile = false;
                tiles.remove(&pos);
            }

            /*[ ][x]*/
            pos = [x1,y0];
            if let Some(tile) = tiles.get(&pos) {
                if self.entity.collide_x(tiles, tile, pos, delta) {
                    //println!("Collided 0,1");
                    rem_tile = tile.collectable;
                }
                if self.entity.collide_y(tiles, tile, pos, delta) {
                    //println!("Collided 0,1");
                    rem_tile = tile.collectable;
                }
            }
            if rem_tile {
                tiles.remove(&pos);
            }
        } // For*/
    } // collision()
}

pub fn update(delta: f64, screen: &mut Screen) {
    {
        let plentity = &mut screen.player.entity;
        plentity.vel[0] = screen.player.keystate.accel(plentity.vel[0], delta, plentity.on_ground);
        if !plentity.on_ground {
            if !screen.player.keystate.contains(JUMP) { // < because it's negative for up
                screen.player.can_jump = true;

                if  plentity.vel[1] < JUMP_MOD {
                    plentity.vel[1] = JUMP_MOD;
                }
            }
        }
            else {
                //plentity.vel[1] = 0.0;

                if screen.player.keystate.contains(JUMP) {
                    if screen.player.can_jump {
                        plentity.vel[1] = JUMP_MOD;
                        plentity.on_ground = false;
                        screen.player.can_jump = false;
                    }
                }
                    else {
                        screen.player.can_jump = true;
                    }
            }
        plentity.vel[1] = screen.player.keystate.grav(plentity.vel[1], delta);
        plentity.pos[0] += plentity.vel[0] * delta;
        plentity.pos[1] += plentity.vel[1] * delta;
    }

    /*if !plentity.on_ground && plentity.pos[1] > 512.0 {
        plentity.pos[1] = 512.0;
        plentity.on_ground = true;
    }
    //if plentity.pos[1] < 512.0-32.0 {
    //    plentity.pos[1] = 512.0-32.0;
    //    plentity.vel[1] = 0.0;
    //}
    if plentity.pos[0] > 2048.0 {
        plentity.pos[0] = 2048.0;
        plentity.vel[0] = 0.0;
    }*/
    let plentity = &mut screen.player.entity;
    plentity.collision(&mut screen.tiles, delta);
    if plentity.vel[1] > 0.0 { // Confirmed for falling
        plentity.on_ground = false;
    }
    //let mut plentity = &mut screen.player.entity;
    //println!("{}", plentity.pos[1]);
}
