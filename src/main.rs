extern crate piston;
extern crate graphics;
extern crate piston_window;
extern crate opengl_graphics;
extern crate current;
extern crate vecmath;
extern crate find_folder;
extern crate fps_counter;

#[macro_use]
extern crate bitflags;

use current::{ Current, CurrentGuard };
//use piston::window::WindowSettings;
//use piston::window::BuildFromWindowSettings;
//use piston::event_loop::*;
//use piston::input::*;
//use piston::input::Button::Keyboard;
//use piston::input::keyboard::Key;
use graphics::Viewport;
use opengl_graphics::{ GlGraphics, OpenGL, TextureSettings, Filter, Texture };
use opengl_graphics::GlyphCache;
use std::rc::Rc;
use std::cell::RefCell;
use std::path::PathBuf;
use std::collections::HashMap;
use camera::Camera;
use screen::Screen;
use world::World;
//use entity::Entity;
use fps_counter::FPSCounter;
use tile::Tile;
use piston_window::*;
use piston_window::Button::Keyboard;
use piston_window::keyboard::Key;
use components::component;
use components::*;
use components::userinput;

//mod player;
mod entity;
mod tile;
mod screen;
mod world;
mod render;
mod camera;
mod components;

const IDEAL_RES: [f64; 5] = [16.0/9.0, 9.0/16.0, 1920.0, 1080.0, 2.0];

///
/// High scope variables to pass between render and update
///
pub struct App<'a> {
    gl: GlGraphics, // OpenGL drawing backend.
    svp: Viewport, // Stored viewport
    world: World<'a>,

    game_state: u8,
    /* Menu states
     * 0 - Main menu
     * 1 - Ingame (World must be set right when called)
     * 2 - Edit mode (Same as ingame?)
     *
     */

    edit_mode: bool,
    cam_snap_i: usize, // Entity index to snap camera to
}

///
/// The main App which deals with user input, rendering, and
/// other window-related tasks. It also holds information about
/// the current game state.
///
impl<'a> App<'a> {
    fn render(&mut self, _args: &RenderArgs, font: &mut GlyphCache) {

        const BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
        match self.game_state {
            1 | 2 => { // Ingame | Edit
                let ref mut world = self.world;
                self.gl.draw(self.svp, |c, gl| { //context, graphics
                    graphics::clear(BLACK, gl);
                    world.screens[world.current_screen].render(&c, gl, &world.tex1);
                    render::text(&c, gl, font);
                });
            },
            _ => { // Default
                self.gl.draw(self.svp, |c, gl| { //context, graphics
                    graphics::clear(BLACK, gl);
                    render::text(&c, gl, font);
                });
            }
        }
    }

    ///
    /// Update all entities and the camera for the current screen
    ///
    fn update(&mut self, args: &UpdateArgs) {
        // TODO: Match draw state

        use current_camera; let cam = current_camera();
        let mut cam = cam.borrow_mut();

        let cur_scr = &mut self.world.screens[self.world.current_screen];
        for entity in &mut cur_scr.entities {
            entity.update(args.dt, &mut cur_scr.tiles);
        }
        cam.update(args.dt);
    }

    ///
    /// Keypress event
    ///
    fn press(&mut self, btn: Button) {
        if btn == Keyboard(Key::Left) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.insert(userinput::LEFT);
                            }
                        }
                    }
                },
                2 => {
                    use current_camera; let cam = current_camera();
                    let mut cam = cam.borrow_mut();

                    cam.keystate.insert(camera::LEFT);
                },
                _ => ()
            }
        }

        if btn == Keyboard(Key::Right) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.insert(userinput::RIGHT);
                            }
                        }
                    }
                },
                2 => {
                        use current_camera; let cam = current_camera();
                        let mut cam = cam.borrow_mut();

                        cam.keystate.insert(camera::RIGHT);
                },
                _ => ()
            }
        }
        if btn == Keyboard(Key::Up) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.insert(userinput::UP);
                            }
                        }
                    }
                },
                2 => {
                        use current_camera; let cam = current_camera();
                        let mut cam = cam.borrow_mut();

                        cam.keystate.insert(camera::UP);
                    },
                _ => ()
            }
        }
        if btn == Keyboard(Key::Down) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.insert(userinput::DOWN);
                            }
                        }
                    }
                },
                2 => {
                        use current_camera; let cam = current_camera();
                        let mut cam = cam.borrow_mut();

                        cam.keystate.insert(camera::DOWN);
                },
                _ => ()
            }
        }

        // TODO: Camera action key

        if btn == Keyboard(Key::A) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.insert(userinput::ACTION);
                            }
                        }
                    }
                },
                _ => ()
            }
        }
        if btn == Keyboard(Key::D) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.insert(userinput::ATTACK);
                            }
                        }
                    }
                },
                _ => ()
            }
        }
        if btn == Keyboard(Key::Space) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.insert(userinput::JUMP);
                            }
                        }
                    }
                },
                _ => ()
            }
        }
        if btn == Keyboard(Key::LShift) || btn == Keyboard(Key::RShift) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.insert(userinput::RUN);
                            }
                        }
                    }
                },
                2 => {
                    use current_camera; let cam = current_camera();
                    let mut cam = cam.borrow_mut();

                    cam.keystate.insert(camera::SPEED);
                },
                _ => ()
            }
        }
    }

    ///
    /// Key release event
    ///
    fn release(&mut self, btn: Button) {
        if btn == Keyboard(Key::Left) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.remove(userinput::LEFT);
                            }
                        }
                    }
                },
                2 => {
                    use current_camera; let cam = current_camera();
                    let mut cam = cam.borrow_mut();

                    cam.keystate.remove(camera::LEFT);
                },
                _ => ()
            }
        }

        if btn == Keyboard(Key::Right) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.remove(userinput::RIGHT);
                            }
                        }
                    }
                },
                2 => {
                    use current_camera; let cam = current_camera();
                    let mut cam = cam.borrow_mut();

                    cam.keystate.remove(camera::RIGHT);
                },
                _ => ()
            }
        }
        if btn == Keyboard(Key::Up) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.remove(userinput::UP);
                            }
                        }
                    }
                },
                2 => {
                    use current_camera; let cam = current_camera();
                    let mut cam = cam.borrow_mut();

                    cam.keystate.remove(camera::UP);
                },
                _ => ()
            }
        }
        if btn == Keyboard(Key::Down) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.remove(userinput::DOWN);
                            }
                        }
                    }
                },
                2 => {
                    use current_camera; let cam = current_camera();
                    let mut cam = cam.borrow_mut();

                    cam.keystate.remove(camera::DOWN);
                },
                _ => ()
            }
        }

        if btn == Keyboard(Key::A) {
            for entity in &mut self.world.screens[self.world.current_screen].entities {
                let co = entity.get_first_component("UserInput");
                if co.is_some()  {
                    if let component::Type::UserInput(c) = co.unwrap().get() {
                        c.keystate.remove(userinput::ACTION);
                    }
                }
            }
        }
        if btn == Keyboard(Key::D) {
            println!("Beep");
            for entity in &mut self.world.screens[self.world.current_screen].entities {
                let co = entity.get_first_component("UserInput");
                if co.is_some()  {
                    if let component::Type::UserInput(c) = co.unwrap().get() {
                        c.keystate.remove(userinput::ATTACK);
                    }
                }
            }
        }
        if btn == Keyboard(Key::Space) {
            for entity in &mut self.world.screens[self.world.current_screen].entities {
                let co = entity.get_first_component("UserInput");
                if co.is_some()  {
                    if let component::Type::UserInput(c) = co.unwrap().get() {
                        c.keystate.remove(userinput::JUMP);
                    }
                }
            }
        }
        if btn == Keyboard(Key::LShift) || btn == Keyboard(Key::RShift) {
            match self.game_state {
                1 => {
                    for entity in &mut self.world.screens[self.world.current_screen].entities {
                        let co = entity.get_first_component("UserInput");
                        if co.is_some()  {
                            if let component::Type::UserInput(c) = co.unwrap().get() {
                                c.keystate.remove(userinput::RUN);
                            }
                        }
                    }
                },
                2 => {
                    use current_camera; let cam = current_camera();
                    let mut cam = cam.borrow_mut();

                    cam.keystate.remove(camera::SPEED);
                },
                _ => ()
            }
        }
        if btn == Keyboard(Key::Tab) {
            use current_camera; let cam = current_camera();
            let mut cam = cam.borrow_mut();
            if self.game_state == 2 {
                self.game_state = 1;
                println!("Edit mode: OFF");
                cam.snap = &self.world.screens[self.world.current_screen].entities[self.cam_snap_i].details;
                cam.clear_keys();
            } else {
                self.game_state = 2;
                println!("Edit mode: ON");
                cam.snap = std::ptr::null();
                //self.world.screens[self.world.current_screen].player.clear_keys();
                for entity in &mut self.world.screens[self.world.current_screen].entities {
                    let co = entity.get_first_component("UserInput");
                    if co.is_some()  {
                        if let component::Type::UserInput(c) = co.unwrap().get() {
                            c.clear_keys();
                        }
                    }
                }
            }
        }
    }

    ///
    /// Window resize event
    ///
    fn resize(&mut self, wh: [u32; 2]) {
        self.svp.window_size = wh;
        self.svp = App::calc_viewport(self.svp);
    }

    ///
    /// Calculate the viewport for the desired window size.
    /// It crops the necessary sides to be within the IDEAL_RES
    ///
    pub fn calc_viewport(mut vp: Viewport) -> Viewport {
        use current_camera; let cam = current_camera();
        let mut cam = cam.borrow_mut();
        /*let mut rectsize: [f64; 2] = [vp.window_size[0] as f64,
                                        (vp.window_size[0] as f64)*IDEAL_RES[1]];*/
        // TODO: Picked option size
        let mut rectsize: [f64; 2] = [1920.0,
            1080.0];
        if (rectsize[1] as u32) > vp.window_size[1] {
            rectsize = [(vp.window_size[1] as f64)*IDEAL_RES[0], vp.window_size[1] as f64];
        }
        vp.rect = [((vp.window_size[0]-(rectsize[0] as u32))/2) as i32,
                    ((vp.window_size[1]-(rectsize[1] as u32))/2) as i32,
                        rectsize[0] as i32, rectsize[1] as i32]; // 16:9

        let scale: [f64; 2] = [(IDEAL_RES[4]*(rectsize[0]/IDEAL_RES[2])), (IDEAL_RES[4]*(rectsize[1]/IDEAL_RES[3]))];

        vp.draw_size = [((vp.window_size[0] as f64)*scale[0]) as u32,
                            ((vp.window_size[1] as f64)*scale[1]) as u32]; // Zoom to scale
        cam.size = [(vp.rect[2] as f64)/scale[0], (vp.rect[3] as f64)/scale[1]]; // Size of screen in "zoomed pixels"
        println!("{:.2} x {:.2}\n{:.2} x {:.2}", cam.size[0], cam.size[1], scale[0], scale[1]);
        /*println!("-- Win --");
        println!("rectsize: {:?} scale: {:?}", rectsize, scale);
        println!("vp.rect: {:?}", vp.rect);
        println!("vp.draw_size: {:?}", vp.draw_size);
        println!("vp.window_size: {:?}", vp.window_size);*/

        vp
    }
}

//pub fn current_world() -> Rc<RefCell<World>> {
//    unsafe {
//        Current::<Rc<RefCell<World>>>::new().clone()
//    }
//}

pub fn current_camera() -> Rc<RefCell<Camera>> {
    unsafe {
        Current::<Rc<RefCell<Camera>>>::new().clone()
    }
}

pub fn current_path() -> Rc<RefCell<PathBuf>> {
    unsafe {
        Current::<Rc<RefCell<PathBuf>>>::new().clone()
    }
}

pub fn current_fps() -> Rc<RefCell<FPSCounter>> {
    unsafe {
        Current::<Rc<RefCell<FPSCounter>>>::new().clone()
    }
}

/*pub fn current_window() -> Rc<RefCell<Window>> {
    unsafe {
        Current::<Rc<RefCell<Window>>>::new().clone()
    }
}*/


///
/// This creates a test world for us.
/// TODO: Load starting world.
///
fn load_world() -> Vec<Screen> {
    let mut world = vec![
        Screen {
            entities: Vec::new(),
            tiles: HashMap::new()
        }
    ];
    let mut e = entity::Entity::new([512.0,512.0]);
    e.components.push(components::userinput::new());
    e.components.push(components::gravity::new());
    e.components.push(components::collision::new());

    world[0].entities.push(e);

    //let i: i32 = 0;
    for i in 0..30 {
        for h in 0..10 {
            world[0].insTile([32*i, 512+64+(32*h)], [0,0], [0,0], [32,32],
                             false, None, 0);
            world[0].insTile([1024+32*i, 512+64+(32*h)], [0,0], [0,0], [32,32],
                             false, None, 0);
        }
        for h in 0..2 {
            world[0].insTile([32*i, 512-128-(32*h)], [0,0], [0,0], [32,32],
                             false, None, 0);
        }

    }
    world[0].insTile([32*7, 480+64], [0,0], [0,0], [32,32],
                     false, None, 0);
    world[0].insTile([32*8, 480+64], [0,0], [0,0], [32,32],
                     false, None, 0); // Little bump

    world[0].insTile([32*1, 480+64], [0,0], [0,0], [32,32],
                     true, None, 0);
    world[0].insTile([32*1, 480+32], [0,0], [0,0], [32,32],
                     true, None, 0);
    world[0].insTile([32*1, 480], [0,0], [0,0], [32,32],
                     true, None, 0);
    for i in 0..7 { // Bottom fall test platform
        world[0].insTile([32*(32+i), 2048], [0,0], [0,0], [32,32],
                         false, None,0);
    }

    world[0].prepare();
    /*world[0].tiles.insert([32*7, 416+64], Tile {
        //pos: [32*7, 0],
        tex: [0,0],
        slope: [0,0],
        col_size: [32,32],
        collectable: true,
    });*/

    // TODO: If [is start screen] set player

    println!("World loaded");

    world
}

///
/// Initializes the game window and holds the main event loop.
///
fn init<'a>(assets: PathBuf) {

        // Change this to OpenGL::V2_1 if not working.
        let opengl = OpenGL::V3_2;

        // let (sw,sh) = glutin::get_primary_monitor().get_dimensions();

        // TODO: Picked option size
        let winsize = [1920,1080];//[IDEAL_RES[2] as u32, IDEAL_RES[3] as u32];

        // Create an Glutin window.
        let mut window: PistonWindow = WindowSettings::new(
                "Gam",
                winsize
            )
            //.decorated(false)
            //.fullscreen(true)
            .opengl(opengl)
            .resizable(false)
        //    .exit_on_esc(true)
            .controllers(false)
            .build()
            .unwrap();
        // nov/13/18 - 570 fps / 1800
        // viewport clipping nov/13/18 - 980 fps / 1800

        window = window.max_fps(200);
        window = window.ups(180); // Updates per second

        //let mut cur_win = Rc::new(RefCell::new(window));
        //let win_guard = CurrentGuard::new(&mut cur_win);

        let ref font = assets.join("prstartk.ttf");
        let mut glyphs = GlyphCache::new(font, (),
                                         TextureSettings::new().filter(Filter::Nearest)
        ).unwrap(); // Convert the font into a usable texture

        let mut scrns = load_world();

        // Create a new game and run it.
        let mut app: App = App {
            gl: GlGraphics::new(opengl),
            svp: App::calc_viewport(Viewport {
                rect: [0; 4],
                draw_size: [0; 2],
                window_size: winsize,
            }),
            world: World {
                screens: &mut scrns,
                current_screen: 0,
                tex1: Texture::from_path(
                   assets.join("tile0.png"),
                   &TextureSettings::new().filter(Filter::Nearest) // If < 2 args, implement manually
                ).unwrap()
            },
            game_state: 1, // Ingame by default
            edit_mode: false,
            cam_snap_i: 0,
        };

        {
                use current_camera;
                let cam = current_camera();
                let mut cam = cam.borrow_mut();

                // Snap our camera to the first entity in the world (Our player)
                cam.snap = &app.world.screens[app.world.current_screen].entities[app.cam_snap_i].details;

                //app.world.screens[app.world.current_screen].player.set_char_default();

                for entity in &mut app.world.screens[app.world.current_screen].entities {
                    entity.set_char_default();
                }
                //cam.size = [app.svp.rect[2] as f64, app.svp.rect[3] as f64];
        }

        // let mut events = Events::new(EventSettings::new().max_fps(180).ups(181));
        // The main event loop
        while let Some(e) = window.next() {
            if let Some(r) = e.render_args() {
                app.render(&r, &mut glyphs);
            } else if let Some(u) = e.update_args() {
                app.update(&u);
            } else if let Some(pr) = e.press_args() {
                app.press(pr);
            } else if let Some(rl) = e.release_args() {
                app.release(rl);
            } else if let Some(rs) = e.resize_args() {
                app.resize(rs);
            }
        }

        //drop(win_guard);
}

fn main() {
    let mut cur_cam = Rc::new(RefCell::new(Camera::new())); // Returns a 'Camera' struct that is initialized.
    let cam_guard = CurrentGuard::new(&mut cur_cam);

    let mut cur_fps = Rc::new(RefCell::new(FPSCounter::new())); // Returns a 'Player' struct that is initialized.
    let fps_guard = CurrentGuard::new(&mut cur_fps);

    let cpath = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets")
                                                            .unwrap();
    let cpathc = cpath.clone();
    let mut cur_path = Rc::new(RefCell::new(cpath));
    let path_guard = CurrentGuard::new(&mut cur_path);

    init(cpathc);

    drop(fps_guard);
    drop(cam_guard);
    drop(path_guard);
}
