use entity::Details;
use std;

const MOVE_SPEED: [f64; 2] = [250.0, 250.0];

bitflags! {
    pub flags KeyState: u8 {
        const LEFT = 0b1,
        const RIGHT = 0b10,
        const UP = 0b100,
        const DOWN = 0b1000,
        const ACTION = 0b10000,
        const SPEED = 0b100000,
    }
}

impl KeyState {
    pub fn move_side(&self) -> f64 {
        let mut acc = 0.0;
        if self.contains(LEFT) {
            if self.contains(SPEED) {
                acc = -MOVE_SPEED[0]*2.0;
            } else {
                acc = -MOVE_SPEED[0];
            }
        }
        if self.contains(RIGHT) {
            if self.contains(SPEED) {
                acc = MOVE_SPEED[0]*2.0;
            } else {
                acc = MOVE_SPEED[0];
            }
        }
        acc
    }
    pub fn move_updwn(&self) -> f64 {
        let mut acc = 0.0;
        if self.contains(UP) {
            if self.contains(SPEED) {
                acc = -MOVE_SPEED[1]*2.0;
            } else {
                acc = -MOVE_SPEED[1];
            }
        }
        if self.contains(DOWN) {
            if self.contains(SPEED) {
                acc = MOVE_SPEED[1]*2.0;
            } else {
                acc = MOVE_SPEED[1];
            }
        }
        acc
    }
}

pub struct Camera {
    pub snap: *const Details,
    pub pos: [f64; 2], // x,y
    pub size: [f64; 2],
    pub keystate: KeyState,
}


impl Camera {
    pub fn clear_keys(&mut self) {
        self.keystate = KeyState::empty();
    }

    ///
    /// Camera factory.
    /// Returns new default camera.
    ///
    pub fn new() -> Camera {
        Camera {
            snap: std::ptr::null(),
            pos: [0.0; 2],
            size: [0.0; 2],
            keystate: KeyState::empty()
        }
    }

    ///
    /// Camera update function.
    ///
    pub fn update(&mut self, delta: f64) {
        let snap: &Details = unsafe { &*self.snap };
        if !(self.snap.is_null()) {
            self.pos[0] = snap.pos[0] + (snap.size[0]/2.0);
            self.pos[1] = snap.pos[1] + (snap.size[1]/2.0);
        }
        else {
            //println!("snap is null");
            self.pos[0] += self.keystate.move_side()*delta;
            self.pos[1] += self.keystate.move_updwn()*delta;
        }
        // TODO: Add other world dimensions later?
        // Snap the X axis to the left of the screen
        if (self.pos[0] - (self.size[0]/2.0)) < 0.0 {
            self.pos[0] = self.size[0]/2.0;
        }

        // Snap the Y axis to the top of the screen
        if (self.pos[1] - (self.size[1]/2.0)) < 0.0 {
            self.pos[1] = self.size[1]/2.0;
        }
    }
}
