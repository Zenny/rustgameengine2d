use opengl_graphics::Texture;
use std::collections::HashMap;
use tile::Tile;
use components::component::Component;
use components::component;
use piston_window::TextureSettings;
use piston_window::Filter;

pub struct Details {
    pub pos: [f64; 2], // x,y top-left
    pub vel: [f64; 2],
    pub tex: Texture,
    pub size: [f64; 2],
    pub on_ground: bool,
    pub can_jump: bool,
}

pub struct Entity {
    pub details: Details,
    pub components: Vec<Box<Component>>,
}

///
/// An entity is a living being that can potentially move around and interact with the world.
///
impl Entity {

    ///
    /// Call update in all of the entity's components.
    ///
    pub fn update(&mut self, delta: f64, tiles: &mut HashMap<[u32; 2], Tile>) {

        //self.collision(tiles, delta);
        for comp in &mut self.components {
            if !comp.run(&mut self.details, delta) {
                if let component::Type::Collision(c) = comp.get() {
                    c.update(&mut self.details, delta, tiles);
                }
            }
        }
    }

    ///
    /// Gets the first component within the entity, if there is one.
    /// Returns an Option containing the first component if available.
    ///
    pub fn get_first_component(&mut self, _type: &str) -> Option<&mut Box<Component>> {
        for c in &mut self.components {
            if c.type_of() == _type {
                return Some(c);
            }
        }
        None
    }

    ///
    /// Entity creation factory
    /// Returns a default entity with only a set position.
    ///
    pub fn new(p: [f64; 2]) -> Entity {
        Entity {
            details: Details {
                tex: Texture::new(0,0,0), /*::empty() doesn't work. Also see: set_char_default() */
                size: [32.0,64.0],
                pos: p,
                vel: [0.0, -0.0],
                on_ground: false,
                can_jump: false,
            },
            components: Vec::new(),
        }
    }

    ///
    /// Sets the texture of the entity to the default player texture.
    ///
    pub fn set_char_default(&mut self) {
        use current_path; let path = current_path();
        let path = path.borrow_mut();
        //let assets = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();
        //println!("{:?}", &assets);
        let pl = path.join("pl.png");
        self.details.tex = Texture::from_path(
            &pl,
            &TextureSettings::new().filter(Filter::Nearest)
        ).unwrap();
        println!("Loaded char");
    }

    /*
        pub fn collide_x(&mut self, tiles: &HashMap<[u32; 2], Tile>, tile: &Tile,
                         pos: [u32; 2], delta: f64) -> bool {
            let e_right = self.pos[0] + self.size[0];
            let e_left = self.pos[0];
            let t_right = pos[0] as f64 + tile.col_size[0] as f64;
            let t_left = pos[0] as f64;
            let mut ret = false;

            if (e_left < t_right && e_left > t_left) && // Left within left/right
                (e_left - self.vel[0] * delta).round() >= t_right && // Moving us back pushes us out
                tiles.get(&[t_right as u32, pos[1]]).is_none() { // No block where we're going
                //println!("Left X");
                if !tile.collectable {
                    self.pos[0] = t_right;
                    self.vel[0] = 0.0;
                }
                ret = true;
            }

            if (e_right < t_right && e_right > t_left) && // Right within left/right
                (e_right - self.vel[0] * delta).round() <= t_left && // Moving us back pushes us out
                tiles.get(&[pos[0] - (tile.col_size[0] as u32), pos[1]]).is_none() { // No block where we're going
                //println!("Right X");
                if !tile.collectable {
                    self.pos[0] = t_left - self.size[0];
                    self.vel[0] = 0.0;
                }
                ret = true;
            }

            ret
        }
        pub fn collide_y(&mut self, tiles: &HashMap<[u32; 2], Tile>, tile: &Tile,
                         pos: [u32; 2], delta: f64) -> bool {
            let e_bot = self.pos[1] + self.size[1]; // - is up
            let e_top = self.pos[1];
            let t_bot = pos[1] as f64 + tile.col_size[1] as f64;
            let t_top = pos[1] as f64;
            let mut ret = false;

            if (e_top < t_bot && e_top > t_top) && // Top within top/bot [- is up]
                (e_top - self.vel[1] * delta).round() >= t_bot && // Moving us back pushes us out
                tiles.get(&[pos[0], t_bot as u32]).is_none() { // No block where we're going
                //println!("Top Y");
                if !tile.collectable {
                    self.pos[1] = t_bot;
                    self.vel[1] = 0.0;
                }
                ret = true;
            }

            if (e_bot < t_bot && e_bot > t_top) && // Bot within top/bot [- is up]
                (e_bot - self.vel[1] * delta).round() <= t_top && // Moving us back pushes us out
                tiles.get(&[pos[0], pos[1] - (tile.col_size[1] as u32)]).is_none() { // No block where we're going
                //println!("Bot Y [{}, {}]", pos[0], pos[1]);
                if !tile.collectable {
                    self.pos[1] = t_top - self.size[1];
                    self.vel[1] = 0.0;
                    self.on_ground = true;
                }
                ret = true;
            }

            ret
        }*/
}
