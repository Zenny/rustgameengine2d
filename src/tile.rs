use entity::Details;

#[derive(Clone)]
pub struct Tile {
  //  pub pos: [u32; 2],
    pub tex: [u8; 2], // 255
    pub slope: [u8; 2], // if 0,0 then not slope
    pub col_size: [u8; 2],
    pub collectable: bool,
    pub col: [bool; 4],
    pub ttype: u8,
}

///
/// A tile is a collidable square (or slope) in the world.
///
impl Tile {

    ///
    /// Compares an entity to the tile to see if they're colliding on the x axis.
    /// Returns whether or not the entity is colliding with the tile on x.
    ///
    pub fn collide_x(&mut self, entity: &mut Details, pos: [u32; 2], delta: f64) -> bool {
        let e_right = entity.pos[0] + entity.size[0];
        let e_left = entity.pos[0];
        let t_right = pos[0] as f64 + self.col_size[0] as f64;
        let t_left = pos[0] as f64;
        let mut ret = false;

        if (e_left < t_right && e_left > t_left) && self.col[1] && // Left within left/right
            (e_left - entity.vel[0] * delta).round() >= t_right // Moving us back pushes us out
        {
            //println!("Left X");
            if !self.collectable {
                entity.pos[0] = t_right;
                entity.vel[0] = 0.0;
            }
            ret = true;
        }

        if (e_right < t_right && e_right > t_left) && self.col[3] && // Right within left/right
            (e_right - entity.vel[0] * delta).round() <= t_left  // Moving us back pushes us out
        {
            //println!("Right X");
            if !self.collectable {
                entity.pos[0] = t_left - entity.size[0];
                entity.vel[0] = 0.0;
            }
            ret = true;
        }

        ret
    }

    ///
    /// Compares an entity to the tile to see if they're colliding on the y axis.
    /// Returns whether or not the entity is colliding with the tile on y.
    ///
    pub fn collide_y(&mut self, entity: &mut Details, pos: [u32; 2], delta: f64) -> bool {
        let e_bot = entity.pos[1] + entity.size[1]; // - is up
        let e_top = entity.pos[1];
        let t_bot = pos[1] as f64 + self.col_size[1] as f64;
        let t_top = pos[1] as f64;
        let mut ret = false;

        if (e_top < t_bot && e_top > t_top) && self.col[2] && // Top within top/bot [- is up]
            (e_top - entity.vel[1] * delta).round() >= t_bot // Moving us back pushes us out
        {
            //println!("Top Y");
            if !self.collectable {
                entity.pos[1] = t_bot;
                entity.vel[1] = 0.0;
            }
            ret = true;
        }

        if (e_bot < t_bot && e_bot > t_top) && self.col[0] && // Bot within top/bot [- is up]
            (e_bot - entity.vel[1] * delta).round() <= t_top // Moving us back pushes us out
        {
            //println!("Bot Y [{}, {}]", pos[0], pos[1]);
            if !self.collectable {
                entity.pos[1] = t_top - entity.size[1];
                entity.vel[1] = 0.0;
                entity.on_ground = true;
            }
            ret = true;
        }

        ret
    }
}